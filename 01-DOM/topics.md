# VS Code extensions
## Node
`sudo apt install nodejs` v12

`npm install -g jsdom`
## Quokka
`Press Ctrl+k -> Q`

# Topics
## 100% Theory
- What is the DOM
- Main data types
- Data types hierarchy
- Available API objects
- DOM Tree

## Practice
- Elements
  - Finding elements
    - Single element
    - Element collections
    - document.querySelector()
    - Static and live collections
    - Find elements from higher parent's branch
  - Adding elements
    - Create new element
    - Create new child element
    - Insert in position
    - References to existing nodes (append existing node elsewhere)
  - Removing elements
    - Remove element
    - Remove child element
    - Remove every inner child element
  - Changing elements
    - innerHTML, innerText and textContent
    - Replace an element
- Attributes
  - Add attributes
  - Remove attributes
  - Change attributes