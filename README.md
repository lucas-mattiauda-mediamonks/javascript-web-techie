# README #

This project aims to help non-FrontEnd developers in better understanding the foundations of JavaScript, in order approach to other technologies like VueJS, ReactJS and others.

## How do I navigate this repository? ##

The repository serves as collection of various topics, theory entries and exercises. Each chapter is represented in branches with the following naming structure:

* **origin/##-{chapterName}**
    * Where "##" represents the order number of the chapter. 

Also, each chapter has two variations:

* origin/##-{chapterName}/**empty-exercises**
    * These branches stand for empty files used to practice during the live meetings while lerning new topics.

* origin/##-{chapterName}/**solved-exercises**
    * These branches stand for exercises which where solved during previous live meetings and which can be used as a knowledge base.

### The "topics.md" file

Finally, you can locate a "topics.md" file inside the chapter's base folder in the repository's sources. This file has a guideline of subjects to explore in order to learn, practice and then move on to the next chapter.

## How do I get set up? ##

* Install Visual Studio Code
* Install Node.js (v11.10.0 or later)
* Install the VS Code extension "Quokka.js" to run JavaScript code inside the code editor.